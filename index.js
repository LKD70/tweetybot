'use strict';

const
	config = require('./config.json'),
	format = require('string-format'),
	fs = require('fs'),
	request = require('request-promise-native'),
	sleep = require('system-sleep'),
	Twitter = require('twitter'),
	{ promisify } = require('util');

format.extend(String.prototype);

const client = new Twitter({
	access_token_key: config.twitter.access_token_key,
	access_token_secret: config.twitter.access_token_secret,
	consumer_key: config.twitter.consumer_key,
	consumer_secret: config.twitter.consumer_secret
});

const sendMessage = (tweet, channel) => {
	const entities = typeof tweet.extended_entities === 'undefined' ? tweet.entities : tweet.extended_entities;
	channel.telegrams.forEach(telegram => {

		const options = {
			method: 'GET',
			qs: {
				chat_id: telegram,
				parse_mode: 'markdown'
			},
			uri: `https://api.telegram.org/bot${config.telegram.token}/`
		};
		if (typeof entities.media === 'undefined') {
			options.uri += 'sendMessage';
			options.qs.text = config.telegram.format.format(tweet);
		} else {
			entities.media.forEach(media => {
				if (config.telegram.media) {
					if (media.type) {
						if (media.type === 'photo') {
							options.uri += 'sendPhoto';
							options.qs.caption = config.telegram.format.format(tweet);
							options.qs.photo = media.media_url_https;
						} else if (media.type === 'video') {
							options.uri += 'sendVideo';
							options.qs.caption = config.telegram.format.format(tweet);
							options.qs.video = media.video_info.variants
								.filter(v => v.content_type === 'video/mp4')
								.sort((a, b) => b.bitrate - a.bitrate)[0].url;
						}
					}
				} else {
					options.uri += 'sendMessage';
					options.qs.text = config.telegram.format.format(tweet);
				}
			});
		}
		request(options);
	});

	channel.discords.forEach(discord => {
		const options = {
			body: {
				content: config.discord.format.format(tweet),
				embeds: []
			},
			json: true,
			method: 'POST',
			uri: discord
		};
		if (config.discord.media) {
			entities.media.forEach(media => {
				if (media.type === 'photo') {
					options.body.embeds.push({
						image: {
							url: media.media_url_https
						}
					});
				} else if (media.type === 'video') {
					options.body.embeds.push({
						video: {
							url: media.video_info.variants
								.filter(v => v.content_type === 'video/mp4')
								.sort((a, b) => b.bitrate - a.bitrate)[0].url
						}
					});
				}
			});
		}

		request(options);
	});
};

async function check() {
	const data = await promisify(fs.readFile)('./channels.json')
		.then(JSON.parse)
		.catch(() => ({}));

	config.channels.forEach(channel => {
		let channel_data = {};
		if (channel.name in data) {
			channel_data = data[channel.name];
		}
		const options = {
			exclude_replies: true,
			include_rts: false,
			screen_name: channel.name,
			trim_user: true
		};
		if ('last' in channel_data) {
			options.since_id = channel_data.last;
		} else {
			options.count = config.count;
			channel_data.last = 0;
		}

		client.get('statuses/user_timeline', options)
			.then((tweets) => {
				for (let t = tweets.length - 1; t > -1; t--) {
					const tweet = tweets[t];
					if (Number(tweet.id) > Number(channel_data.last)) {
						sendMessage(tweet, channel);
						sleep(100);
						if (t === 0) {
							channel_data.last = tweet.id;
							data[channel.name] = channel_data;
							promisify(fs.writeFile)('./channels.json', JSON.stringify(data));
						}
					}
				}
			});
	});
}

check();
setInterval(check, config.sleep * 60 * 1000);
