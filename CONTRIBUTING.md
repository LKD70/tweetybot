# Contributing to tweetybot

tweetybot is built and maintained by LKD70, though it is welcome to outside help.
I encourage anyone who wishes to contribute to open issues and make pull requests.


## Ways you can help

### Financial Support

Though it doesn't cost me any aditional money to produce and run this project, if some some reason you feel you'd like to make a monitary contribution, you can do so using [PayPal](https://paypal.me/LKDVPS).

### Pull Requests

I welcome anyone to open a pull request. All I ask is you follow the eslint rules and provide documentation and reason for your changes, thank you in advance!

### Documentation

Feel free to make suggestions regarding the documentation, or take it upon yourself to document anything you wish through a pull request.

### Suggestions

I'd love to hear your suggestions, but before submitting a suggestion, ensure it doesn't already exist.

To check if a suggestion already exists, [search the open issues](issues). If you find a suggestion you like, add a :thumbsup: or comment if you'd like to recommend changes.

[Create a new issue](https://gitlab.com/LKD70/tweetybot/issues/new)
