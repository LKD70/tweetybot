[![Donate](https://img.shields.io/badge/donate-paypal-blue.svg)](https://www.paypal.me/LKDVPS) [![Discord](https://img.shields.io/discord/407163464474296331.svg?logo=discord&maxAge=60)](https://discord.gg/jHuCH6J) [![Telegram Follow](https://img.shields.io/badge/Telegram-LKD70-ff0066.svg)](https://t.me/LKD70) [![Twitter Follow](https://img.shields.io/twitter/follow/espadrine.svg?style=social&label=Follow)](https://twitter.com/lkdornan70)

# Tweetybot
Tweetybot is a small bot used to post tweets to both Telegram and Discord; Written in [Node.js](https://nodejs.org). The bot supports multiple Twitter channels at once, as well as mutliple discord and telegram servers/channels/groups.

## Powered by
Tweetybot wouldn't be chirping if it wasn't for a few other  projects...

[![NPM](https://nodei.co/npm-dl/request-promise-native.png)](https://npmjs.com/package/request-promise-native)
[![NPM](https://nodei.co/npm-dl/string-format.png)](https://npmjs.com/package/string-format)
[![NPM](https://nodei.co/npm-dl/twitter.png)](https://npmjs.com/package/twitter)
[![NPM](https://nodei.co/npm-dl/system-sleep.png)](https://npmjs.com/package/system-sleep)


## Installation

This bot requires [Node.js](https://nodejs.org) in order to run.
Download the project from our git & install dependencies:
```sh
$ git clone https://gitlab.com/LKD70/tweetybot.git
$ cd tweetybot
$ npm install
$ cp config.example.json config.json
$ rm config.example.json
```

## Configuration
In order to use tweetybot, you must first configure it.
Configuration is handled by the `config.json` file.

Example `config.json`:

```json
{
    "sleep": 10,
    "count": 5,
    "telegram": {
        "token": "123456789:a1b2c3d4e5f6g7h8i9j0klmnopqrstuvwxyz",
		"format": "{text}",
		"media": true
    },
    "discord": {
        "format": "{text}",
        "media": true
    },
    "twitter": {
        "consumer_key": "aBcDeFgHiJkLmNoP",
        "consumer_secret": "1abc2def3ghi4jkl5mno6pqr7stu8vwx9yz0",
        "access_token_key": "0987654321-0zy9xwv8uts7rqp6onm5lkj4ihg3fed2cba1",
        "access_token_secret": "Ab1Cd2Ef3Gh4Ij5Kl6Mn7Op8Qr9St0Uv9Wx8Yz7"
    },
    "channels": [
        {
            "name": "Username",
            "discords": [
                "https://discordapp.com/api/webhooks/01928374656574839201/Aa1Bb2Cc3Dd4Ee5Ff6Gg7Hh8Ii9Jj0Kk9Ll8Mm7Nn6Oo5Pp4Qq3Rr2Ss1Tt0Uu1Vv2Ww3Xx4Yy5Zz6"
            ],
            "telegrams": [
                "-100200300400"
            ]
        }
    ]
}

```

### Configuration explinations
| Element | Example Data | Description |
| ----- | ----- | ----- |
| sleep | 10 | Time to sleep in between checks. |
| count | 5 | Amount of tweets to grab if no "last" tweet is found. |
| telegram.token | 123456789:a1b2c3d4e5f6g7h8i9j0klmnopqrstuvwxyz | Your Telegram bots token |
| telegram.format | {text} | String format using data from twitter API. See an [example response](https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline). |
| telegram.media | true | Enable sending of media (videos/photos). |
| discord.format | {text} |  String format using data from twitter API. See an [example response](https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline). |
| discord.media | true | Enable sending of media (videos/photos). |
| twitter.consumer_key | aBcDeFgHiJkLmNoP | Your twitter consumer_key. See [Twitter authentication](#twitter-authentication) for more. |
| twitter.consumer_secret | 1abc2def3ghi4jkl5mno6pqr7stu8vwx9yz0 | Your twitter consumer_secret. See [Twitter authentication](#twitter-authentication) for more. |
| twitter.access_token_key | 0987654321-0zy9xwv8uts7rqp6onm5lkj4ihg3fed2cba1 | Your twitter access_token_key. See [Twitter authentication](#twitter-authentication) for more. |
| twitter.access_token_secret | Ab1Cd2Ef3Gh4Ij5Kl6Mn7Op8Qr9St0Uv9Wx8Yz7 | Your twitter access_token_secret. See [Twitter authentication](#twitter-authentication) for more. |
| channels | [] | An array of channels. |
| channels[].name | PicturesFoIder | A channel name. |
| channels[].discords | discordapp.com/api/webhooks/... | A discord webhook link. See [Discord Authentication](#discord-authentication) for more. |
| channels[].telegrams | -12345678909876 | A Telegram `chat_id`, see [Telegram Authentication](#telegram-authnetication) for more. |

### Twitter authentication
The bot uses Twitters oAuth for authentication.

1. Visit: https://apps.twitter.com and create a new app.
2. Fill out the required fills, agree to the agreement, and create the App.
3. Navigate to the Keys and Access Tokens tab.
4. Copy the API key and API secret in to the `config.json` file.
5. Under "Token Actions", Create a new token.
6. Copy the Access token and Access token secret in to the `config.json` file.

### Discord authentication
Discord authentication is handled through their webhooks. A webhook can be created following these instructions:

1. Right click your Discord server and select Server Settings.
2. Select the 'Webhooks' tab.
3. Click 'Create Webhook'.
4. Name the webhook and select the channel you wish it to post to.
5. Copy the webhook URL and add it to the `config.json` file.

### Telegram authentication
Telegram authentication uses a Telegram bot, to create a bot follow these steps:

1. Talk to https://t.me/BotFather, Create a `/newbot`, and follow the instructions.
2. Add the token to the `config.json` file.
3. Add the bot as an administrator in the channel/group you wish it to post to.
4. Send a message in the channel/group and use [getUpdates](https://core.telegram.org/bots/api#getupdates) to find the channel/groups `chat_id` and add it to the `config.json` file.


## License

[GNU Affero General Public License v3.0](LICENSE)


## Contributors

If you'd like to contribute to this project, please check out the  [Contributer guidelines](CONTRIBUTING.md).
